Construido para crear cartas de yu gi oh al estilo orica del anime... Hecho JSE para PC y J2ME para CEL, la interfaz grafica de celular fue hecha gracias a la libreria extraida de https://docs.oracle.com/javame/dev-tools/jme-sdk-3.2/ecl/html/lwuit.htm
<hr>

<p align="center">
  <img alt="Light" src="https://gitlab.com/RicardoValladares/Yu-Gi-Oh/-/raw/main/ScreenShoot-PC.png" width="40%">
  &nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Dark" src="https://gitlab.com/RicardoValladares/Yu-Gi-Oh/-/raw/main/ScreenShoot-CEL.png" width="40%">
</p>

